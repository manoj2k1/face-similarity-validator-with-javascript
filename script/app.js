const cam = document.getElementById('webcam');

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/model')
]).then(() => {
  navigator.getUserMedia(
    {
      video: {}
    },
    mediaStream => cam.srcObject = mediaStream,
    err => console.log(err)
  )
});

cam.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(cam);
  document.body.append(canvas);
  const camArea = { 
    width: cam.width,
    height: cam.height
  }
  faceapi.matchDimensions(canvas, camArea);
  setInterval(async () => { 
    const detections = await faceapi.detectAllFaces(cam, new faceapi.TinyFaceDetectorOptions());
    const resizedDetections = faceapi.resizeResults(detections, camArea);
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    faceapi.draw.drawDetections(canvas, resizedDetections);
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
  }, 100);
})